# README #

Welcome to the rt-n56u project

This project aims to improve the rt-n56u and other supported devices on the software part, allowing power user to take full control over their hardware.
This project was created in hope to be useful, but comes without warranty or support. Installing it will probably void your warranty. 
Contributors of this project are not responsible for what happens next.

### How do I get set up? ###

* [Get the tools to build the system](https://bitbucket.org/padavan/rt-n56u/wiki/EN/HowToMakeFirmware) or [Download pre-built system image](https://bitbucket.org/padavan/rt-n56u/downloads)
* Feed the device with the system image file (Follow instructions of updating your current system)
* Perform factory reset
* Open web browser on http://my.router to configure the services


                         BUILD INSTRUCTIONS
```
# create directory for images
mkdir ~/mybuild
docker build -t padavan .
docker run --it -v ~/mybuild:/mnt/share santaklouse/padavan 
```

inside container:
```
cd /opt/padavan/toolchain
./build_toolchain.sh

cd ../trunk
# copy your router config template to trunk path
cp ./configs/templates/asus/rt-ac51u.config ./.config
./clear_tree.sh
./build_firmware.sh
cp images/*.trx /mnt/share
```
Done.



### Contribution guidelines ###

* To be completed

P.S.
Original project author is Padavan. Old repository here: [rt-n56u project](https://bitbucket.org/padavan/rt-n56u)
